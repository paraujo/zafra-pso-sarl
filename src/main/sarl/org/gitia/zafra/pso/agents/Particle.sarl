package org.gitia.zafra.pso.agents

import io.sarl.core.DefaultContextInteractions
import io.sarl.core.Initialize


import utils.Matriz
import utils.SharedValues
import pso.general.capacidades.EvaluarFuncionObjetivo
import pso.general.capacidades.EvaluarFuncionObjetivoSkill
import pso.general.capacidades.CalculosPSO
import pso.general.capacidades.CalculosPSOSkill
import java.util.Random

/** 
 * 
 */
/** 
 * @author Pedro Bernabé Araujo
 * 
 */
agent Particle {
	uses DefaultContextInteractions

	var localBest : double
	var currentValue : double
	var localBestPosition : Matriz = new Matriz(SharedValues.getNumeroFilas(), SharedValues.getNumeroColumnas())
	var currentPosition : Matriz = new Matriz(SharedValues.getNumeroFilas(), SharedValues.getNumeroColumnas())
	var currentSpeed : Matriz = new Matriz(SharedValues.getNumeroFilas(), SharedValues.getNumeroColumnas())
	var newSpeed : Matriz = new Matriz(SharedValues.getNumeroFilas(), SharedValues.getNumeroColumnas())
	var Aproduccion : Matriz = new Matriz(SharedValues.getNumeroFilas(), SharedValues.getNumeroColumnas())
	var Pbeneficios : Matriz = new Matriz(SharedValues.getNumeroFilas(), SharedValues.getNumeroColumnas())
	var maxProduction : Matriz = new Matriz(1, SharedValues.getNumeroColumnas())
	var rnd : Random = new Random();
	var NormaVelocidad : double
	var percentage : double = 0.0

	uses EvaluarFuncionObjetivo, DefaultContextInteractions, CalculosPSO

	on Initialize {

		SharedValues.setIdGuardado(getID())
		setSkill(EvaluarFuncionObjetivo, new EvaluarFuncionObjetivoSkill)
		setSkill(CalculosPSO, new CalculosPSOSkill)
		Aproduccion.LeerMatrizProduccion() // cargo la matriz produccion de un archivo
		Pbeneficios.LeerMatrizBeneficios()
		maxProduction.LeerProduccionMaxima()
		println("Particula Creada")
		currentPosition.GenerarMatrizRandom()

		currentValue = FuncionObjetivo(Pbeneficios, currentPosition, Aproduccion, maxProduction)
		println("valor con la funcion esta que me dieron : " + currentValue)
		currentSpeed.GenerarVelocidadRandom()

		localBest = currentValue
		localBestPosition = currentPosition

		emit(new actionParticle(currentValue, currentPosition, percentage))

	}

	on perceptionSwarm {

		emit(new resetSwarm())
		Thread.sleep(330) // ver bien esto		
		
		newSpeed = CalcularNuevaVelocidad(InfluenciaVelocidadActual(2.5, currentSpeed),
			InfluenciaLocalBest(1.3, localBestPosition, currentPosition),
			InfluenciaGlobalBest(1.3, occurrence.globalBestPosition, currentPosition))
		currentSpeed = NormalizarSigma(newSpeed)
		currentSpeed.Normalizar()

		currentPosition = CalcularNuevaPosicion(currentPosition, currentSpeed)
		currentValue = FuncionObjetivo(Pbeneficios, currentPosition, Aproduccion, maxProduction)
		percentage = ProcentajeGlobalBest(currentPosition, occurrence.globalBestPosition)

		if (currentValue > localBest) {
			localBest = currentValue
			localBestPosition.copiar(currentPosition)
		}

		println("valor Actual de cada particula: " + currentValue + " valor del porcentaje : " + percentage)
		emit(new actionParticle(currentValue, currentPosition, percentage))
	}

}
