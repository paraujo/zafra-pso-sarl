package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Random;

public  class Matriz {
 
	private double[][] Matriz;
	
 	private int N;
	
	private int M;
	
	public Matriz(int N, int M){
	this.Matriz = new double [N][M];
	this.N=N;
	this.M=M;
	
	}

	public int getN(){
		return this.N;
	}
	
	public int getM(){
		return this.M;
	}
	
	public double getElemento(int i, int j){
		return this.Matriz[i][j];
	}
	
	
	public void setElemento(int i, int j, double valor){
		this.Matriz[i][j]=valor;
	}
	
	public void mostrar(){
		for(int k=0; k<this.N; k++){
			for(int h=0; h<this.M;h++){
				System.out.printf("%.5f ", this.Matriz[k][h] );
				
			}
			System.out.println();
		}
	}
	
	public void Dividir(double div) {
		if (div != 0) {
			for (int i = 0; i <this.getN(); i++) {
				for (int j = 0; j < this.getM(); j++) {
					this.setElemento(i, j, (this.getElemento(i, j))/div);
				}
			}
		}
	}
	
	
	public void GenerarMatrizRandom(){
		 Random rnd = new Random();
		 double valor;
		 int k=0;
		 double mayor=-999.0;
		for(int i=0; i<this.N;i++){
			for (int j=0;j<this.M;j++){
				valor=rnd.nextDouble();
				if(j==0){
					mayor=valor;
					k=j;
					this.setElemento(i, j, 1);
				}
				else{
				if(valor>mayor){
				mayor=valor;
				this.setElemento(i,k,0.0);
				this.setElemento(i,j,1.0);
				k=j;
				}
					else{
					this.setElemento(i,j,0.0);	
					}
				}
				//if(valor>0.5) this.setElemento(i, j, 1);
				//else  this.setElemento(i, j, 0);
			}
		}
		
	}
	
	public void GenerarMatrizZero(){

		for(int i=0; i<N;i++){
			for (int j=0;j<M;j++){
				this.Matriz[i][j]=0.0;
			}
		}
		
	}

	public void GenerarVelocidadRandom(){
		 Random rnd = new Random();
		 
		for(int i=0; i<this.getN();i++){
			for (int j=0;j<this.getM();j++){
			this.setElemento(i, j, rnd.nextDouble());
			}
		}
		
	}
	
	public double NormaVelocidad(){
		double suma =0;
		for(int i=0; i<this.getN();i++){
			for (int j=0;j<this.getM();j++){
			suma = suma + Math.pow(this.getElemento(i, j), 2.0);
			}
		}
		suma = Math.sqrt(suma);
		return suma;
	}
	
	public void Normalizar(){
		double Vmax =Math.sqrt(this.getM()*this.getN());
		double NormaVelocidadActual=0;
		
		for(int i=0; i<this.getN();i++){
			for (int j=0;j<this.getM();j++){
			NormaVelocidadActual = NormaVelocidadActual + Math.pow(this.getElemento(i, j), 2.0);
			}
		}
		NormaVelocidadActual= Math.sqrt(NormaVelocidadActual);
		
		if(NormaVelocidadActual>Vmax){
			this.Dividir(NormaVelocidadActual/Vmax);
		}
		
	}
	
	public void LeerMatrizProduccion() {

		//String direccionArchivo = "/home/pedro/workspaceSARL-2/zafra-pso-sarl/data/B312.txt"; // direccion		
		String direccionArchivo = System.getProperty("user.dir");		
		try {

			BufferedReader bf = new BufferedReader(new FileReader(direccionArchivo + "/data/B312.txt"));
			System.out.println(bf.readLine());
			String temp = "";
			double valor = 0;
			ArrayList<Double> valores = new ArrayList<Double>();
			int k = 0;
			String bfRead;
			while ((bfRead = bf.readLine()) != null) { // haz el ciclo, mientras
														// bfRead tiene datos
				for (int i = 0; i < bfRead.length(); i++) {
					if (bfRead.charAt(i) != ' ' && i != bfRead.length()) {
						temp = temp + bfRead.charAt(i);

					} else {

						valor = Double.parseDouble(temp);
						temp = "";
						valores.add(valor);
					}
				}
				valor = Double.parseDouble(temp);
				temp = "";
				valores.add(valor);
			}

			for (int h = 0; h < this.getN(); h++) {
				for (int w = 0; w < this.getM(); w++) {
					this.setElemento(h, w, (Double) valores.get(k));
					k++;

				}
			}
		} catch (Exception e) {
			System.err.println("No se encontro archivo de produccion");
		}

	}
	
	public void LeerMatrizBeneficios() {

		//String direccionArchivo = "/home/pedro/workspaceSARL-2/zafra-pso-sarl/data/B312.txt"; // direccion
		String direccionArchivo = System.getProperty("user.dir");

		try {

			BufferedReader bf = new BufferedReader(new FileReader(direccionArchivo + "/data/B312.txt"));
			String temp = "";
			double valor = 0;
			ArrayList<Double> valores = new ArrayList<Double>();
			int k = 0;
			String bfRead;
			while ((bfRead = bf.readLine()) != null) { // haz el ciclo, mientras
														// bfRead tiene datos

				for (int i = 0; i < bfRead.length(); i++) {
					if (bfRead.charAt(i) != ' ') {
						temp = temp + bfRead.charAt(i);

					} else {

						valor = Double.parseDouble(temp);
						temp = "";
						valores.add(valor);

					}
				}

				valor = Double.parseDouble(temp);
				temp = "";
				valores.add(valor);
			}
			
			for (int h = 0; h < this.getN(); h++) {
				for (int w = 0; w < this.getM(); w++) {
					this.setElemento(h, w, 1.0/ valores.get(k));
					k++;
				}
			}

		} catch (Exception e) {
			System.err.println("No se encontro archivo Beneficios");
		}

	}
	
	
	public void LeerProduccionMaxima(){
		
		//String direccionArchivo = "/home/pedro/workspaceSARL-2/zafra-pso-sarl/data/B312.txt"; // direccion
		String direccionArchivo = System.getProperty("user.dir");
		try {

			BufferedReader bf = new BufferedReader(new FileReader(direccionArchivo + "/data/B312.txt"));
			String temp = "";
			double valor = 0;
			int k = 0;
			String bfRead;
			ArrayList<Double> valores = new ArrayList<Double>();
			while ((bfRead = bf.readLine()) != null) { // haz el ciclo, mientras
														// bfRead tiene datos


				for (int i = 0; i < bfRead.length(); i++) {
					if (bfRead.charAt(i) != ' ') {
						temp = temp + bfRead.charAt(i);

					} else {

						valor = Double.parseDouble(temp);
						temp = "";
						valores.add(valor);

					}
				}

				valor = Double.parseDouble(temp);
				temp = "";
				valores.add(valor);
			}
			
			
				for (int j = 0; j < this.getM(); j++) {
					this.setElemento(0, j, valores.get(k));
					k++;
				}
			


		} catch (Exception e) {
			System.err.println("No se encontro archivo de ProduccionMaxima");
		}
	}

/*
	public void LeerMatrizPrueba() {

		String direccionArchivo = "D:\\DOCUMENTOS\\facultad\\PROYECTOS\\ArchivosParaCargarDatos\\MatrizPruebaA312.txt"; // direccion

		try {

			BufferedReader bf = new BufferedReader(new FileReader(direccionArchivo));
			String temp = "";
			double valor = 0;
			ArrayList<Double> valores = new ArrayList<Double>();
			int k = 0;
			String bfRead;
			while ((bfRead = bf.readLine()) != null) { // haz el ciclo, mientras
														// bfRead tiene datos

				for (int i = 0; i < bfRead.length(); i++) {
					if (bfRead.charAt(i) != ' ') {
						temp = temp + bfRead.charAt(i);

					} else {

						valor = Double.parseDouble(temp);
						temp = "";
						valores.add(valor);

					}
				}

				valor = Double.parseDouble(temp);
				temp = "";
				valores.add(valor);
			}
			
			for (int h = 0; h < this.getN(); h++) {
				for (int w = 0; w < this.getM(); w++) {
					this.setElemento(h, w,valores.get(k));
					k++;
				}
			}

		} catch (Exception e) {
			System.err.println("No se encontro archivo");
		}

	}*/
	
	public void copiar(Matriz copia){
		for(int i=0; i<copia.getN(); i++)
			for(int j=0; j<copia.getM();j++)
				this.setElemento(i, j, copia.getElemento(i, j));
	}
	
	
	
}
